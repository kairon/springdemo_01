package com.gientech.springExercise.controller;

import org.springframework.web.bind.annotation.*;

/**
 * @Author KAiRON
 * @Date 2023/2/9 16:58
 * @Version 1.0
 */
@RestController
public class DemoController {

    @RequestMapping("/test")
    public String test(){
        return "收到请求了。";
    }
}
